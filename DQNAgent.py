#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Aug  2 14:12:07 2019

@author: szary
"""
from __future__ import division
import numpy as np
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.optimizers import Adam
from random import sample

class Agent():
    def __init__(self):
        self.learning_rate = 0.00001
        self.gamma = 0.9
        self.epsilon = 150
        #self.model = self.qmodel()
        self.model = self.qmodel('weights29.hdf5') #if you want to use your weights, make sure to comment upper statement
        self.short_memory = []
        self.memory = []
        
    #initialize the model
    def qmodel(self, weights = None):
        model = Sequential([
            Dense(24,  activation = 'relu', input_dim = 24),
            Dropout(0.15),
            Dense(120, activation = 'relu'),
            Dropout(0.15),
            Dense(120, activation = 'relu'),
            Dropout(0.15),
            Dense(4, activation = 'softmax')])
    
        opt = Adam(self.learning_rate)
        model.compile(optimizer = opt, loss = 'mse')
        if weights:
            model.load_weights(weights)
        return model

    #set reward as 10 for eating an apple, -10 for dying and 0 otherwise
    def get_reward(self, player):
        self.reward = -0.1
        if not player.alive:
            self.reward = -10
            return self.reward
        if player.eaten:
            self.reward = 10
        return self.reward    

    #get current environment state    
    def get_state(self, game, player, apple):

        diagonal = (game.win_width**2 + game.win_height**2)**(0.5)
        
        found = False
        dr_found = ddr_found = dd_found = ddl_found = dl_found = dul_found = du_found = dur_found = False 
        danger_right = danger_dright = danger_down = danger_dleft = danger_left = danger_uleft = danger_up = danger_uright = 0
        i = 1
        while not found and i < game.win_height:         
            for j in player.position[1:]:

                if player.x + i*20 == game.win_width and dr_found == False:
                    dr_found = True
                    danger_right = 0
                else:
                    if j == [player.x + i*20, player.y] and dr_found == False:    
                        dr_found = True
                        danger_right = (j[0] - player.x) / game.win_width
                
                if (player.x + i*20 == game.win_width or player.y + i*20 == game.win_height) and ddr_found == False:
                    ddr_found = True
                    danger_dright = 0
                else:
                    if j == [player.x + i*20, player.y + i*20] and ddr_found == False:
                        ddr_found = True 
                        danger_dright = ((j[0] - player.x)**2 + (j[1] - player.y)**2)**(0.5) / diagonal
                    
                if player.y + i*20 == game.win_height and dd_found == False:
                    dd_found = True 
                    danger_down = 0
                else:
                    if j == [player.x, player.y + i*20] and dd_found == False:
                        dd_found = True 
                        danger_down = (j[1] - player.y) / game.win_height
                
                if (player.x - i*20 == -20 or player.y + i*20 == game.win_height) and ddl_found == False:
                    ddl_found = True
                    danger_dleft = 0
                else:
                    if j == [player.x - i*20, player.y + i*20] and ddl_found == False:
                        ddl_found = True
                        danger_dleft = ((player.x - j[0])**2 + (j[1] - player.y)**2)**(0.5) / diagonal 
                
                if player.x - i*20 == -20 and dl_found == False:
                    dl_found = True
                    danger_left = 0
                else:
                    if j == [player.x - i*20, player.y] and dl_found == False:
                        dl_found = True 
                        danger_left = (player.x - j[0]) / game.win_width
                    
                if (player.x - i*20 == -20 or player.y - i*20 == -20) and dul_found == False:
                    dul_found = True 
                    danger_uleft = 0
                else:
                    if j == [player.x - i*20, player.y - i*20] and dul_found == False:
                        dul_found = True
                        danger_uleft = ((player.x - j[0])**2 + (player.y - j[1])**2)**(0.5) / diagonal
                    
                if player.y - i*20 == -20 and du_found == False:
                    du_found = True
                    danger_up = 0
                else:
                    if j == [player.x, player.y - i*20] and du_found == False:
                        du_found = True
                        danger_up = (player.y - j[1]) / game.win_height
                    
                if (player.x + i*20 == game.win_width or player.y - i*20 == -20) and dur_found == False: 
                    dur_found = True
                    danger_uright = 0
                else:
                    if j == [player.x + i*20, player.y - i*20] and dur_found == False:
                        dur_found = True
                        danger_uright = ((j[0] - player.x)**2 + (player.y - j[1])**2)**(0.5) / diagonal
                        
            if dr_found and ddr_found and dd_found and ddl_found and dl_found and dul_found and du_found and dur_found:
                break
            
            i += 1
                    
        if apple.x - player.x > 0 and apple.y - player.y > 0:
            bottom_right = ((apple.x - player.x)**2 + (apple.y - player.y)**2)**(0.5) / diagonal
        else:
            bottom_right = 0
            
        if apple.x - player.x < 0 and apple.y - player.y > 0:
            bottom_left = ((apple.y - player.y)**2 + (player.x - apple.x)**2)**(0.5) / diagonal
        else:
            bottom_left = 0
        
        if apple.x - player.x < 0 and apple.y - player.y < 0:
            upper_left = ((player.x - apple.x)**2 + (player.y - apple.y)**2)**(0.5) / diagonal
        else:
            upper_left = 0
            
        if apple.x - player.x > 0 and apple.y - player.y < 0:
            upper_right = ((apple.x - player.x)**2 + (player.y - apple.y)**2)**(0.5) / diagonal
        else:
            upper_right = 0
        
            
        state = [
                np.array_equal(player.direction, [0, 0, 0, 1]), #going right
                np.array_equal(player.direction, [0, 0, 1, 0]), #going left
                np.array_equal(player.direction, [0, 1, 0, 0]), #going down
                np.array_equal(player.direction, [1, 0, 0, 0]), #going up
                
                (game.win_width - 20 - player.x) / game.win_width, #distance to the right wall
                player.x / game.win_width, #distance to the left wall
                (game.win_height - 20 - player.y) / game.win_height, #distance to the bottom wall
                player.y / game.win_height, #distance to the upper wall
                
                #normalized distances to the apple
                max(0, (apple.x - player.x) / game.win_width), #right distance
                bottom_right, #bottom right distance 
                max(0, (apple.y - player.y) / game.win_height), #bottom distance
                bottom_left, #bottom left distance
                max(0, (player.x - apple.x) / game.win_width), #left distance
                upper_left, #upper left distance
                max(0, (player.y - apple.y) / game.win_height), #upper distance
                upper_right, #upper right distance
                
                #normalized distances to own body parts
                danger_right, 
                danger_dright,
                danger_down,
                danger_dleft,
                danger_left,
                danger_uleft,
                danger_up,
                danger_uright
                ]
        return np.asarray(state)
    
    #add to q-table
    def remember(self, state, action, reward, new_state, alive):
        self.memory.append((state, action, reward, new_state, alive))
        
    #train using q-learning algorithm
    def trainmemory(self, memory):
        if len(memory) > 1000:
            minibatch = sample(memory, 1000)
        else:
            minibatch = memory
        for state, action, reward, new_state, alive in minibatch:
            target = reward
            if alive:
                target = reward + self.gamma * np.amax(self.model.predict(np.array([new_state]))[0])
            target_f = self.model.predict(np.array([state]))
            target_f[0][np.argmax(action)] = target
            self.model.fit(np.array([state]), target_f, epochs = 1, verbose = 0)
        
    def train_shortmemory(self, state, action, reward, new_state, alive):
        target = reward 
        if alive:
            target = reward + self.gamma * np.amax(self.model.predict(np.array([new_state]))[0])
        target_f = self.model.predict(np.array([state]))
        target_f[0][np.argmax(action)] = target
        self.model.fit(state.reshape((1, 24)), target_f, epochs = 1, verbose = 0)
        
        
        
        
        
        
        