# Snake game using Deep Reinforcement Learning Algorithm

## Introduction
My interest in AI began after seeing genetic algorithm playing snake, so I decided to code my own model. For this project I used Keras framework.

## Model architecture
I did not want to teach the agent by playing snake myself. Instead, the agent itself had to figure out what are the rules, and achieve highest number of points.
I decided to use the following architecture:

```python
    class Agent():
        def __init__(self):
            self.learning_rate = 0.001
            self.gamma = 0.9
            self.epsilon = 150
            self.model = self.qmodel()
            #self.model = self.qmodel('weights.hdf5') #if you want to use your weights, make sure to comment upper statement
            self.short_memory = []
            self.memory = []
            
        #initialize the model
        def qmodel(self, weights = None):
            model = Sequential([
                Dense(24,  activation = 'relu', input_dim = 24),
                Dropout(0.15),
                Dense(120, activation = 'relu'),
                Dropout(0.15),
                Dense(120, activation = 'relu'),
                Dropout(0.15),
                Dense(4, activation = 'softmax')])
        
            opt = Adam(self.learning_rate)
            model.compile(optimizer = opt, loss = 'mse')
            if weights:
                model.load_weights(weights)
            return model
```

The model has 24 inputs. It is able to 'see' in four directions. For each direction, it sees the distance between the wall, the apple (if any), the body part (if any) and its head. For greater spatial knowledge, it is also able to see diagonally, between the apple, body parts and its head. Furthermore, the first four inputs are the direction the snake is currently moving. That is 4 inputs (for each movement direction) + 4 (for each wall) + 8 (for apple in each direction) + 8 (for body part in each direction). The output of the network is just the next direction a model thinks the snake should go.

## Training the agent
I used quite popular Q-learning algorithm. It allows the model to gain knowledge about the environment it is playing in. The algorithm looks like following:

![](img/qlearning.png)

As the snake explores the environment, it fills the Q-table, allowing it to choose the best action in a given situation in future.

The training itself goes as follows:

```python
    def trainmemory(self, memory):
        if len(memory) > 1000:
            minibatch = sample(memory, 1000)
        else:
            minibatch = memory
        for state, action, reward, new_state, alive in minibatch:
            target = reward
            if alive:
                target = reward + self.gamma * np.amax(self.model.predict(np.array([new_state]))[0])
            target_f = self.model.predict(np.array([state]))
            target_f[0][np.argmax(action)] = target
            self.model.fit(np.array([state]), target_f, epochs = 1, verbose = 0)
```

Basically, as the algorithm does different actions, we either punish it (for going into its own body part or into a wall, bad snake!) or rewarding it with a cookie for gaining a point (eating an apple, good snake!). To allow exploration, we set epsilon = 150 to let the agent sometimes do random actions in the first 150 games. The Agent was trained using 1000 games, although it began gaining more than 3 points after 100 games already.

## Result after training
I've made a [video] demonstrating how the agent did after training.
[video]: https://www.youtube.com/watch?v=Pv4nwkAGsL8&feature=youtu.be
