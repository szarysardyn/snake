#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 18 15:44:29 2019

@author: szary
"""

import pygame as pg
from random import randint
import numpy as np
from DQNAgent import Agent
from keras.utils import to_categorical
import time

#game class with game rules
class game:
    
    def __init__(self):
        self.win_width = 500
        self.win_height = self.win_width
        pg.init()
        pg.display.set_caption('Snake')
        self.screen = pg.display.set_mode((self.win_width, self.win_height))
        self.screen.fill((255, 255, 255))
        self.player = player(self)
        self.apple = apple(self)
        pg.display.flip()
        
    def wall_collision(self, position):
        if position[0] == -20 or position[0] == self.win_width or position[1] == -20 or position[1] == self.win_height:
            self.player.alive = False
    
    def collision(self, position):
        for i in range(3, self.player.length):
            if position[0] == position[i + 1]:
                self.player.alive = False
       
        
#player class to handle movement and eating an apple
class player(object):
    
    def __init__(self, game):
        self.alive = True
        self.eaten = False
        self.length = 3
        self.snake_image = pg.image.load('img/snakeskin.png')
        self.x = int(game.win_width/20 * 0.2) * 20
        self.y = int(game.win_height/20 * 0.2) * 20
        self.x_change = 20
        self.y_change = 0
        self.position = []
        self.position.append([self.x, self.y])
        for i in range(self.length):
            self.position.append([self.x, self.y])
        self.direction = []
    
    #display snake's body segments
    def player_display(self, screen):
        for i in range(len(self.position)):
            screen.blit(self.snake_image, (self.position[i]))
            
    #check if apple coordinates and snake head coordinates are the same 
    def eat_apple(self, head_position, apple_coordinates, apple, game):
        #if so, add 1 to snake's length and call new_apple function
        if head_position == apple_coordinates:
            self.eaten = True
            self.position.append(self.position[self.length - 1])
            self.length += 1
            apple.new_apple(game)
    
    #handle movement        
    def player_move(self, direction):
        if self.eaten:
            self.eaten = False
            
        if np.array_equal(direction, [1, 0, 0, 0]) and self.y_change != 20:
            self.x_change = 0
            self.y_change = -20
            self.direction = direction

        elif np.array_equal(direction, [0, 1, 0, 0]) and self.y_change != -20:
            self.x_change = 0
            self.y_change = 20
            self.direction = direction
            
        elif np.array_equal(direction, [0, 0, 1, 0]) and self.x_change != 20:
            self.x_change = -20
            self.y_change = 0
            self.direction = direction
        
        elif np.array_equal(direction, [0, 0, 0, 1]) and self.x_change != -20:
            self.x_change = 20
            self.y_change = 0
            self.direction = direction
        
        self.x += self.x_change
        self.y += self.y_change
        
        self.update_position(self.x, self.y)
               
    #update snake's body segments positions
    def update_position(self, x, y):
        self.position[0] = [x, y]
        for i in reversed(range(1, self.length + 1)):
            self.position[i] = self.position[i - 1]
        
        
#apple class 
class apple(object):
    
    def __init__(self, game):
        self.x, self.y = self.new_apple(game)
        self.apple_image = pg.image.load('img/apple.png')
    
    #generate new apple, if it is in any snake's body segment, call the function again 
    def new_apple(self, game):
        self.x = randint(0, game.win_width/20 - 1) * 20
        self.y = randint(0, game.win_height/20 - 1) * 20
        if [self.x, self.y] not in game.player.position:
            return self.x, self.y
        else:
            return self.new_apple(game)
    
    #display the apple        
    def apple_display(self, screen):
        screen.blit(self.apple_image, (self.x, self.y))

#function on loop to display background, player and apple
def loop_display(game):
    game.screen.fill((255, 255, 255))
    game.player.player_display(game.screen)
    game.apple.apple_display(game.screen)    
    pg.display.update()
    
def run():
    games = 0
    record = 0
    agent = Agent()
    while games < 10000:
        game1 = game()
        player = game1.player
        apple = game1.apple
        while player.alive:
            loop_display(game1)
        
            pg.event.pump()
            keys = pg.key.get_pressed()
            
            if keys[pg.K_ESCAPE]:   
                player.alive = False
                pg.quit()

            #remove the apostrophes if you want to play by yourself                
            '''    
            elif keys[pg.K_UP]:
                player.direction = [1, 0, 0, 0]
                    
            elif keys[pg.K_DOWN]:
                player.direction = [0, 1, 0, 0]    
                        
            elif keys[pg.K_LEFT]:
                player.direction = [0, 0, 1, 0]
                            
            elif keys[pg.K_RIGHT]:
                player.direction = [0, 0, 0, 1]
            '''
            
            ''
            
            #get current enviroment state
            state = agent.get_state(game1, player, apple)
            '''
            #perform random movements based on the agent's epsilon to allow it to explore the environment
            if randint(0, 200) < agent.epsilon - games:
                player.direction = to_categorical(randint(0, 2), num_classes = 4)
            
            #perform an action based on the neural network otuput
            else:'''
            prediction = agent.model.predict(state.reshape(1, 24))
            player.direction = to_categorical(np.argmax(prediction), num_classes = 4)
            ''
            
            #player movement
            player.player_move(player.direction)        
            #check snake's head and apple coordinates
            player.eat_apple(player.position[0], [apple.x, apple.y], apple, game1)
            #check snake's collision with itself
            game1.collision(player.position)
            #check wall collision
            game1.wall_collision(player.position[0])
            
            
            ''
            #get new state after moving, and set reward
            #new_state = agent.get_state(game1, player, apple)
            #reward = agent.get_reward(player)
            
            #train based on taken action and and add new element to the q-table
            #agent.train_shortmemory(state, player.direction, reward, new_state, player.alive)
            #agent.remember(state, player.direction, reward, new_state, player.alive)
            ''
            
            pg.display.flip()
            time.sleep(0.1)
                    
        print('Number of games: ' + str(games + 1) + ' Score: ' + str(player.length - 3))
        games += 1
        
        #whenever agent sets new record, save its weights
        if player.length - 3 > record:
            record = player.length - 3
            string = 'weights' + str(record) + '.hdf5'
            agent.model.save_weights(str(string))
        
        #train from q-table
        agent.trainmemory(agent.memory)
        
    print('Biggest number of points gained by your snake is: ' + str(record) + '!')
    agent.model.save_weights('weightslast.hdf5' )
    
run()